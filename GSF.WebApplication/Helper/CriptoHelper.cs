﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace GSF.WebApplication.Helper
{
    public static class CriptoHelper
    {
        public static string Encrypt(string senha)
        {
            var senhaInformada = Encoding.UTF8.GetBytes(senha);
            var senhaCripto = SHA256.Create().ComputeHash(senhaInformada);

            var hashString = string.Empty;
            foreach (byte x in senhaCripto)
            {
                hashString += String.Format("{0:x2}", x);
            }

            return hashString;
        }
    }
}