﻿using System;

namespace GSF.WebApplication.Models
{
    public class Turno
    {
        public int Inicio { get; set; }
        public int Fim { get; set; }
    }
}