﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GSF.WebApplication.Models
{
    public class FuncionarioViewModel
    {
        public string Nome { get; set; }

        public int Idade { get; set; }

        public AreaAtuacao AreaAtuacao { get; set; }

        public Cargo Cargo { get; set; }

        public DateTime DataAdmissao { get; set; }

        public HorarioTrabalho HorarioTrabalho { get; set; }
    }
}