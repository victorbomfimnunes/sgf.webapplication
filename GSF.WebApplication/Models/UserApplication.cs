﻿using System;

namespace GSF.WebApplication.Models
{
    public class UserApplication
    {
        public Guid Guid { get; set; }
        public string Login { get; set; }
        public string Token { get; set; }
        public string Roles { get; set; }
    }
}