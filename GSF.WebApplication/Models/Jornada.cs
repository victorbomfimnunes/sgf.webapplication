﻿using System;

namespace GSF.WebApplication.Models
{
    public class Jornada
    {
        public int Id { get; set; }
        public TimeSpan Entrada { get; set; }
        public TimeSpan Saida { get; set; }
    }
}