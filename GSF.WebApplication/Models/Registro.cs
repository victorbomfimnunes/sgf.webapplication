﻿using System;

namespace GSF.WebApplication.Models
{
    public class Registro
    {
        public Guid Guid { get; set; }
        public DateTime Horario { get; set; }
    }
}