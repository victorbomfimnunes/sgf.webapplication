﻿using System;

namespace GSF.WebApplication.Models
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Idade { get; set; }
        public AreaAtuacao AreaAtuacao { get; set; }
        public Cargo Cargo { get; set; }
        public DateTime DataAdmissao{ get; set; }
        //public virtual JornadaTrabalho JornadaTrabalho { get; set; }
    }
}