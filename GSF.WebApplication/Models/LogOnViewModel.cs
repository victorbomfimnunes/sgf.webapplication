﻿namespace GSF.WebApplication.Models
{
    public class LogOnViewModel
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public string ErrorMessage { get; set; }
    }
}