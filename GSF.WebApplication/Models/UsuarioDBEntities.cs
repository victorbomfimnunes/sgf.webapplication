﻿using System.Data.Entity;

namespace GSF.WebApplication.Models
{
    public partial class UsuarioDBEntities : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Usuario> Usuarios { get; set; }
    }
}