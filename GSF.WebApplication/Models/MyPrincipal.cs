﻿using System.Security.Principal;

namespace GSF.WebApplication.Models
{
    public class MyPrincipal : GenericPrincipal
    {
        public MyPrincipal(IIdentity identity, string[] roles)
        : base(identity, roles)
        {
        }
        public string UserDetails { get; set; }
    }
}