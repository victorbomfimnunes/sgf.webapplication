﻿using System;

namespace GSF.WebApplication.Models
{
    public class TokenViewModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string roles { get; set; }
        public Guid guid { get; set; }
    }
}