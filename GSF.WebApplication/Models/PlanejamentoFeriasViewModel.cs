﻿using System;

namespace GSF.WebApplication.Models
{
    public class PlanejamentoFeriasViewModel
    {
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
    }
}