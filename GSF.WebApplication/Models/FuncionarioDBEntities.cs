﻿using System.Data.Entity;

namespace GSF.WebApplication.Models
{
    public partial class FuncionarioDBEntities : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Funcionario> Funcionarios { get; set; }
    }
}