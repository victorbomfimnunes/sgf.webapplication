﻿namespace GSF.WebApplication.Models
{
    public class HorarioTrabalho
    {
        private int Id { get; set; }
        private Turno primeiroTurno { get; set; }
        private Turno segundoTurno { get; set; }

        public HorarioTrabalho(Turno primeiroTurno, Turno segundoTurno)
        {
            this.primeiroTurno = primeiroTurno;
            this.segundoTurno = segundoTurno;
        }
    }
}