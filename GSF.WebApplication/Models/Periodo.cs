﻿using System;

namespace GSF.WebApplication.Models
{
    public class Periodo
    {
        public DateTime Inicio { get; set; }
        public DateTime Fim { get; set; }
    }
}