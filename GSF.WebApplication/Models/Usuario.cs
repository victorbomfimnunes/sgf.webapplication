﻿namespace GSF.WebApplication.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] SenhaCripto { get; set; }
    }
}