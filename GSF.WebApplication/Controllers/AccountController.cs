﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Net.Mail;
using System.Net;
using System;
using RestSharp;
using System.Threading;
using GSF.WebApplication.Models;
using System.Security.Principal;
using System.Threading.Tasks;
using GSF.WebApplication.Helper;
using System.Web.Script.Serialization;

namespace GSF.WebApplication.Controllers
{
    public class AccountController : Controller
    {
        private UsuarioDBEntities db = new UsuarioDBEntities();
        private JavaScriptSerializer serializador = new JavaScriptSerializer();
        private UserApplication usuario;

        public ActionResult LogOn()
        {
            return View();
        }

        public ActionResult Profile()
        {
            return View(usuario);
        }

        public ActionResult Forgot()
        {
            return View();
        }

        public ActionResult ForgotPassword([Bind(Include = "Email")] LogOnViewModel viewModel)
        {
            var url = string.Format("http://localhost:6734/api/usuarios/recuperarsenha/{0}", viewModel.Email);
            var result = HttpHelper.Get(url);

            var usr = serializador.Deserialize<Usuario>(result);

            if (usr.Email == viewModel.Email)
            {
                var vm = new LogOnViewModel
                {
                    Email = usr.Email
                };

                return RedirectToAction("ChangePassword", vm);
            }

            return View();
        }

        public ActionResult ChangePassword(LogOnViewModel usuario)
        {
            return View(usuario);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmarSenha([Bind(Include = "Email, Senha")] LogOnViewModel viewModel)
        {
            var senhaInformada = Encoding.Unicode.GetBytes(viewModel.Senha);
            var senhaCripto = SHA256.Create().ComputeHash(senhaInformada);

            using (var entities = new UsuarioDBEntities())
            {
                var usuario = entities.Usuarios.FirstOrDefault(x => x.Email == viewModel.Email);
                usuario.SenhaCripto = senhaCripto;

                entities.Entry(usuario).State = System.Data.Entity.EntityState.Modified;

                //4. call SaveChanges
                entities.SaveChanges();
            }

            return RedirectToAction("LogOn");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOn([Bind(Include = "Login, Senha")] LogOnViewModel viewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {

                var client = new RestClient("http://localhost:6734/api/security/token");

                var request = new RestRequest(Method.POST);
                request.AddParameter("grant_type", "password");
                request.AddParameter("username", viewModel.Login);
                request.AddParameter("password", CriptoHelper.Encrypt(viewModel.Senha));

                var response = client.Execute<TokenViewModel>(request);
                var token = response.Data.access_token;
                var roles = response.Data.roles;
                var guid = response.Data.guid;

                if (!string.IsNullOrWhiteSpace(token))
                {
                    var usuario = new UserApplication
                    {
                        Login = viewModel.Login,
                        Token = token,
                        Roles = roles,
                        Guid = guid
                    };

                    Session["usuario"] = usuario;

                    FormsAuthentication.SetAuthCookie(token, false);

                    var principal = new MyPrincipal(new GenericIdentity(token), new string[] { "myRole" });
                    principal.UserDetails = token;
                    Thread.CurrentPrincipal = principal;
                    System.Web.HttpContext.Current.User = principal;

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                            && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("CustomError", "Usuário ou senha inválidos.");
                }
            }

            return View();
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Usuario,Email,Senha")] LogOnViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var senhaInformada = Encoding.Unicode.GetBytes(viewModel.Senha);
                var senhaCripto = SHA256.Create().ComputeHash(senhaInformada);

                var usuario = new Usuario
                {
                    Email = viewModel.Email,
                    SenhaCripto = senhaCripto
                };

                db.Usuarios.Add(usuario);
                db.SaveChanges();
                return RedirectToAction("LogOn");
            }

            return View(viewModel);
        }

        private static void EnviarEmail(string from)
        {
            var senhaTemporaria = GeraSenha();

            var msg = new MailMessage
            {
                From = new MailAddress("victorbnunes88@gmail.com"),
                Subject = "Recuperação de Senha",
                Body = ("Sua senha temporária é:" + senhaTemporaria),
                IsBodyHtml = true
            };

            msg.To.Add(from);

            using (var smt = new SmtpClient())
            {
                var ntwd = new NetworkCredential
                {
                    UserName = "victorbnunes88@gmail.com",
                    Password = ""
                };
                smt.Host = "smtp.gmail.com";
                smt.UseDefaultCredentials = true;
                smt.Credentials = ntwd;
                smt.Port = 587;
                smt.EnableSsl = true;
                smt.Send(msg);
            }
        }

        private static string GeraSenha()
        {
            var guid = Guid.NewGuid().ToString().Replace("-", "");

            var clsRan = new Random();
            var tamanhoSenha = clsRan.Next(6, 18);

            var senha = "";
            for (var i = 0; i <= tamanhoSenha; i++)
            {
                senha += guid.Substring(clsRan.Next(1, guid.Length), 1);
            }

            return senha;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
