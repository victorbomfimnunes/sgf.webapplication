﻿using GSF.WebApplication.Helper;
using GSF.WebApplication.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace GSF.WebApplication.Controllers
{
    [Authorize]
    public class FuncionariosController : Controller
    {
        private FuncionarioDBEntities db = new FuncionarioDBEntities();
        private JavaScriptSerializer serializador = new JavaScriptSerializer();
        private UserApplication usuario;

        public FuncionariosController()
        {
            usuario = (UserApplication)Session["usuario"];
        }

        public ActionResult Cadastrar()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar([Bind(Include = "Nome,Idade,AreaAtuacao,Cargo,DataAdmissao,HorarioTrabalho")] FuncionarioViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var funcionario = BuildFuncionario(viewModel);
                var result = HttpHelper.Post("http://localhost:6734/api/funcionarios", funcionario);
                
                var funcionarios = serializador.Deserialize<ICollection<Funcionario>>(result);
                return RedirectToAction("Listar", funcionarios);
            }

            return View();
        }

        private static Funcionario BuildFuncionario(FuncionarioViewModel viewModel)
        {
            return new Funcionario
            {
                Nome = viewModel.Nome,
                Idade = viewModel.Idade,
                Cargo = viewModel.Cargo,
                AreaAtuacao = viewModel.AreaAtuacao,
                DataAdmissao = DateTime.Now
            };
        }

        public ActionResult Listar()
        {
            var result = HttpHelper.Get("http://localhost:6734/api/funcionarios");

            var serializador = new JavaScriptSerializer();
            var funcionarios = serializador.Deserialize<ICollection<Funcionario>>(result);

            return View(funcionarios);
        }

        public ActionResult PlanejamentoFerias()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PlanejamentoFerias([Bind(Include = "DataInicio, DataFim")] PlanejamentoFeriasViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var ferias = new Ferias
                {
                    Periodo = new Periodo
                    {
                        Inicio = Convert.ToDateTime(viewModel.DataInicio),
                        Fim = Convert.ToDateTime(viewModel.DataFim)
                    }
                };

                var url = string.Format("http://localhost:6734/api/funcionarios/{0}/ferias", usuario.Guid);
                var result = HttpHelper.Post(url, ferias);

                return RedirectToAction("Index", "Home");
            }

            return View();
        }


        public ActionResult PontoEletronico()
        {
            return View();
        }

        public ActionResult PontoEletronicoDetalhes()
        {
            var url = string.Format("http://localhost:6734/api/pontoeletronico/{0}", usuario.Guid);
            var response = HttpHelper.Get(url);
            var registros = serializador.Deserialize<ICollection<Registro>>(response);

            return View(registros);
        }

        [HttpPost]
        public ActionResult RegistrarPonto(string horario)
        {
            var registro = new Registro
            {
                Guid = usuario.Guid,
                Horario = DateTime.Now
            };

            var response = HttpHelper.Post("http://localhost:6734/api/pontoeletronico", registro);


            return View();
        }
    }
}